$(function(){
	output = false;
	tela.init();
	tela.loading({clear:true})
	database.init().then(jogadores.init);
	frmGetJogadores.init();
	frmEsquema.init();
	
});

var tela = {
	init: function(){
		$(window).on('scroll', function(){
			if (window.scrollY > 100) {
				$(".back-top").removeClass('hidden');
			} else {
				$(".back-top").addClass('hidden');
			}
		});

		$(".back-top").on('click', function(e){
			e.preventDefault();
			$("body, html").stop().animate({scrollTop:0}, 500, 'swing');
		});
	},
	cliqueParaVerInfo: function() {
		$(".list-group-jogadores .indice").unbind('click').on('click', function(e){
			e.preventDefault();
			$(this).parents('.dados').find('.info').toggleClass('open');
		});
	},
	habilitarFormularios: function() {
		$("#frmEsquema button[type=submit]").prop('disabled' ,false);
		$("#frmGetJogadores button[type=submit]").prop('disabled' ,false);
	},
	loading: function(params) {
		var progress = $(".loading .progress-bar");

		if(params.hasOwnProperty('clear') && params.clear == true) {
			progress.html('').css({width: 0}).removeClass('progress-bar-danger');
		}

		if (params.hasOwnProperty('value')) {
			$(".loading").removeClass('hidden');
			progress.css({width: params.value+"%"});
		}

		if (params.hasOwnProperty('valueAdd')) {
			$(".loading").removeClass('hidden');
			var valueAtual = parseInt(progress.css('width').replace('px', ''));
			var valueTotal = parseInt(progress.parent().css('width').replace('px', ''));
			var valueNovo = params.valueAdd + (valueAtual / valueTotal * 100);
			progress.css({width: valueNovo+"%"});
		}

		if (params.hasOwnProperty('msg')) {
			$(".loading").removeClass('hidden');
			progress.html(params.msg);
		}

		if (params.hasOwnProperty('error') && params.error == true) {
			$(".loading").removeClass('hidden');
			progress.addClass('progress-bar-danger');
		} else {
			$(".loading").removeClass('hidden');
			progress.removeClass('progress-bar-danger');
		}

		if (params.hasOwnProperty('close') && params.close == true) {
			setTimeout(function() {
				tela.loading({clear: true}); 
				$(".loading").addClass('hidden');
			}, 1500);
		}
	}
};

var frmEsquema = {
	init: function() {
		$("#frmEsquema").on('submit', function(e){
			e.preventDefault();


			var params = {
				id: $("#frmEsquema .id:checked").val()
			};

			tela.loading({clear: true, value: 40, msg: 'Buscando jogadores'});

			jogadores
				.getJogadoresByEsquema(params)
				.then(function(data){
					tela.loading({valueAdd: 20, msg: 'Processando informações'});
					return data;
				})
				.then(jogadores.listarEsquema)
				.then(function(){
					tela.loading({value: 100, msg: 'Concluído!', close: true});
				})
			;
		});
	}
};

var frmGetJogadores = {
	init: function() {
		$("#frmGetJogadores").on('submit', function(e){
			e.preventDefault();

			var params = {
				posicao_id: $("#frmGetJogadores .posicao_id:checked").val(),
				status_id: $("#frmGetJogadores .status_id:checked").val(),
			};

			tela.loading({clear: true, value: 50, msg: 'Buscando jogadores'});
			
			jogadores.getJogadores(params)
				.then(function(data){
					tela.loading({value: 80, msg: 'Processando informações'});
					return data;
				})
				.then(jogadores.processar)
				.then(jogadores.listar)
				.then(function(){
					tela.loading({value: 100, msg: 'Concluído!', close: true});
				})
			;
		});
	}
};

var output = false;
var log = function(msg) {
	if (output) {
		console.log(msg);
	}
}

var database = {
	db: null,
	dbName: "CartolaDB",
	init: function(){
		return new Promise(function(fullfill){
			var request = window.indexedDB.open(database.dbName, 1);
			tela.loading({value: 25, msg: 'Verificando database'});

			request.onerror = function(event){
				tela.loading({value: 40, msg: 'Erro na database', error: true});
				alert('Erro ao conectar no banco de dados, por favor atualize a página.');
			};
			request.onsuccess = function(event){
				database.db = request.result;
				fullfill();
			};
			request.onupgradeneeded = function(event){
				var db = event.target.result;

				var objectStore = db.createObjectStore("jogadores", {keyPath: "atleta_id"});
				objectStore.createIndex("nome", "nome", {unique: false});
				objectStore.createIndex("apelido", "apelido", {unique: false});
				objectStore.createIndex("clube_id", "clube_id", {unique: false});
				objectStore.createIndex("posicao_id", "posicao_id", {unique: false});
				objectStore.createIndex("status_id", "status_id", {unique: false});
				objectStore.createIndex("posicao_id-status_id", ["posicao_id", "status_id"], {unique: false});
			};
		});
	},
	getDB: function() {
		return database.db;
	},
	getObjectStore: function(objectStoreName, mode) {
		if (mode === undefined) {
			mode = "readonly";
		}

		var db = database.getDB();
		var transaction = db.transaction(["jogadores"], mode);
		var objectStore = transaction.objectStore(objectStoreName);

		return objectStore;
	},
	deleteDatabase: function() {
		var confirm = prompt("Confirma excluir database? Digite 'SIM'", "NAO");
		if(confirm === "SIM") {
			window.indexedDB.deleteDatabase(database.dbName);
		}
	}
};

var jogadores = {
	clubes: null,
	posicoes: null,
	status: null,
	esquemasPossiveis: {
		1: {id:1,descricao:"3-4-3",qtde:{1:1,2:0,3:3,4:4,5:3,6:1}},
		2: {id:2,descricao:"3-5-2",qtde:{1:1,2:0,3:3,4:5,5:3,6:1}},
		3: {id:3,descricao:"4-3-3",qtde:{1:1,2:2,3:2,4:3,5:3,6:1}},
		4: {id:4,descricao:"4-4-2",qtde:{1:1,2:2,3:2,4:4,5:2,6:1}},
		5: {id:5,descricao:"4-5-1",qtde:{1:1,2:2,3:2,4:5,5:1,6:1}},
		6: {id:6,descricao:"5-3-2",qtde:{1:1,2:2,3:3,4:3,5:2,6:1}},
		7: {id:7,descricao:"5-4-1",qtde:{1:1,2:2,3:3,4:4,5:1,6:1}}
	},
	init: function(){
		var template = document.querySelector("#esquema");

		for (var i in jogadores.esquemasPossiveis) {
			var esquema = jogadores.esquemasPossiveis[i];
			var clone = template.content.cloneNode(true);

			$(clone)
				.find('.id')
				.val(esquema.id)
				.after(esquema.descricao)
			;

			template.parentNode.appendChild(clone);
		}

		tela.loading({value: 50, msg: 'Carregando informações'});

		$.ajax({
			url: "api.php?act=/atletas/mercado",
			method: "GET",
			dataType: "json"
		}).done(function(data){
			if (data.hasOwnProperty("atletas")) {
				var objectStore = database.getObjectStore("jogadores", "readwrite");

				tela.loading({value: 75, msg: 'Salvando informações'});

				for (var i in data.atletas) {
					objectStore.put(data.atletas[i]);
				}

				jogadores.loaded = true;
			} else {
				tela.loading({value: 50, msg: 'Falha ao obter as informações', error: 1});
				alert("Falha ao buscar jogadores, por favor recarregue a página.");
				return;
			}

			if (data.hasOwnProperty("posicoes")) {
				jogadores.posicoes = data.posicoes;
				var template = document.querySelector("#posicao");

				for (var i in data.posicoes) {
					var clone = template.content.cloneNode(true);

					$(clone)
						.find('.posicao_id')
						.val(data.posicoes[i].id)
						.after(data.posicoes[i].nome)
					;

					template.parentNode.appendChild(clone);
				}
			}

			if (data.hasOwnProperty("status")) {
				jogadores.status = data.status;
				var template = document.querySelector("#status");

				for (var i in data.status) {
					var clone = template.content.cloneNode(true);

					$(clone)
						.find('.status_id')
						.val(data.status[i].id)
						.after(data.status[i].nome)
					;

					template.parentNode.appendChild(clone);
				}
			}


			if (data.hasOwnProperty("clubes")) {
				jogadores.clubes = data.clubes;
			}

			tela.loading({value: 100, msg: 'Concluído!', close: true});
			tela.habilitarFormularios();
		});
	},
	getJogadores: function(params) {
		return new Promise(function(fullfill){
			var objectStore = database.getObjectStore("jogadores", "readwrite");
			var index = objectStore.index("posicao_id-status_id");
			var retorno = [];

			var values = [
				parseInt(params.posicao_id),
				parseInt(params.status_id)
			];
			
			index.openCursor(IDBKeyRange.only(values)).onsuccess = function(event) {
				var cursor = event.target.result;

				if (cursor) {
					retorno.push(cursor.value);
					cursor.continue();
				} else {
					fullfill(retorno);
				}
			};
		});
	},
	getJogadoresByEsquema: function(params) {
		return new Promise(function(fullfill){
			var posicoes = jogadores.esquemasPossiveis[params.id]['qtde'];
			var promises = [];

			function factoryPromise(id) {
				return Promise.resolve(posicoes[id]).then(function(qtde){
					if (qtde === 0) {
						return [];
					} else {
						return jogadores.getJogadores({posicao_id: id, status_id:7})
							.then(function(data){
								return jogadores.processar(data, qtde);
							});
						}
					});
			}

			for (var i in posicoes) {
				promises.push(factoryPromise.bind(this, i)());
			}

			tela.loading({valueAdd: 30, msg: 'Processando informações'});

			Promise.all(promises).then(fullfill);
		});
	},
	listar: function(data) {
		$(".list-group-jogadores .list-group-item").remove();

		var template = document.querySelector("#list-item-jogador");

		for (var i in data) {
			var classe;
			var jogador = data[i];
			var clone = template.content.cloneNode(true);

			var posicao = jogadores.posicoes[jogador.posicao_id]['abreviacao'].toUpperCase();
			$(clone)
				.find('.list-group-item-heading')
				.html("["+posicao+"] "+jogador.apelido)
			;

			// foto jogador
			$(clone).find('.thumbnail .jogador').attr('src', jogador.foto.replace('FORMATO', '140x140'));

			// foto escudo - jogador
			$(clone)
				.find('.thumbnail .escudo')
				.attr({
					src: jogadores.clubes[jogador.clube_id]['escudos']['60x60'],
					title: jogadores.clubes[jogador.clube_id]['nome']
				})
			;

			// foto escudo - mandante
			$(clone)
				.find('.partida .escudo-mandante')
				.attr({
					src: jogadores.clubes[jogador.partida.clube_casa_id]['escudos']['60x60'],
					title: jogadores.clubes[jogador.partida.clube_casa_id]['nome']
				})
			;

			// posicao - mandante
			$(clone)
				.find('.partida .posicao-mandante')
				.html(jogador.partida.clube_casa_posicao + "º")
			;

			// foto escudo - visitante
			$(clone)
				.find('.partida .escudo-visitante')
				.attr({
					src: jogadores.clubes[jogador.partida.clube_visitante_id]['escudos']['60x60'],
					title: jogadores.clubes[jogador.partida.clube_visitante_id]['nome']
				})
			;

			// posicao - visitante
			$(clone)
				.find('.partida .posicao-visitante')
				.html(jogador.partida.clube_visitante_posicao + "º")
			;

			// pontuacao
			var pontuacao = jogador.pontuacao;

			if (pontuacao < 25) {
				classe = 'label-danger';
			} else if (pontuacao >= 25 && pontuacao < 50) {
				classe = 'label-warning';
			} else if (pontuacao >= 50 && pontuacao < 75) {
				classe = 'label-info';
			} else if (pontuacao >= 75) {
				classe = 'label-success';
			}

			$(clone).find('.indice').html(String(pontuacao).split('.')[0]).addClass(classe);

			//preco 
			$(clone).find('.preco').html(jogador.preco_num);

			// valorizacao
			var valorizacao = jogador.variacao_num;

			if (valorizacao >= 0) {
				classe = 'text-success';
			} else {
				classe = 'text-danger';
			}

			$(clone).find('.valorizacao').html(String(valorizacao)).addClass(classe);

			// media
			var media = jogador.media_num;

			if (media >= 0) {
				classe = 'text-success';
			} else {
				classe = 'text-danger';
			}

			$(clone).find('.media').html(String(media)).addClass(classe);

			template.parentNode.appendChild(clone);
		}

		tela.cliqueParaVerInfo();
	},
	listarEsquema: function(data) {
		$(".list-group-jogadores .list-group-item").remove();
		var template = document.querySelector("#list-item-jogador-esquema");

		for (var i = 0; i < data.length; i++) {
			if (data[i].length > 0) {
				for (var j in data[i]) {
					var jogador = data[i][j];
					var clone = template.content.cloneNode(true);

					// apelido
					var posicao = jogadores.posicoes[jogador.posicao_id]['abreviacao'].toUpperCase();
					$(clone)
						.find('.list-group-item-heading')
						.html("["+posicao+"] "+jogador.apelido)
					;

					// foto jogador
					$(clone).find('.thumbnail .jogador').attr('src', jogador.foto.replace('FORMATO', '140x140'));

					// foto escudo - jogador
					$(clone)
						.find('.thumbnail .escudo')
						.attr({
							src: jogadores.clubes[jogador.clube_id]['escudos']['60x60'],
							title: jogadores.clubes[jogador.clube_id]['nome']
						})
					;

					// foto escudo - mandante
					$(clone)
						.find('.partida .escudo-mandante')
						.attr({
							src: jogadores.clubes[jogador.partida.clube_casa_id]['escudos']['60x60'],
							title: jogadores.clubes[jogador.partida.clube_casa_id]['nome']
						})
					;

					// posicao - mandante
					$(clone)
						.find('.partida .posicao-mandante')
						.html(jogador.partida.clube_casa_posicao + "º")
					;

					// foto escudo - visitante
					$(clone)
						.find('.partida .escudo-visitante')
						.attr({
							src: jogadores.clubes[jogador.partida.clube_visitante_id]['escudos']['60x60'],
							title: jogadores.clubes[jogador.partida.clube_visitante_id]['nome']
						})
					;

					// posicao - visitante
					$(clone)
						.find('.partida .posicao-visitante')
						.html(jogador.partida.clube_visitante_posicao + "º")
					;

					// pontuacao
					var pontuacao = jogador.pontuacao;

					if (pontuacao < 25) {
						classe = 'label-danger';
					} else if (pontuacao >= 25 && pontuacao < 50) {
						classe = 'label-warning';
					} else if (pontuacao >= 50 && pontuacao < 75) {
						classe = 'label-info';
					} else if (pontuacao >= 75) {
						classe = 'label-success';
					}

					$(clone).find('.indice').html(String(pontuacao).split('.')[0]).addClass(classe);

					//preco 
					$(clone).find('.preco').html(jogador.preco_num);

					// valorizacao
					var valorizacao = jogador.variacao_num;

					if (valorizacao >= 0) {
						classe = 'text-success';
					} else {
						classe = 'text-danger';
					}

					$(clone).find('.valorizacao').html(String(valorizacao)).addClass(classe);

					// media
					var media = jogador.media_num;

					if (media >= 0) {
						classe = 'text-success';
					} else {
						classe = 'text-danger';
					}

					$(clone).find('.media').html(String(media)).addClass(classe);

					template.parentNode.appendChild(clone);
				}
			}
		}

		tela.cliqueParaVerInfo();
	},
	processar(data, limit) {
		var retorno = [];

		if (data.length === 0) {
			return retorno;
		}

		for (var i in data) {
			var pontuacao = 50;
			var jogador = data[i];

			if (jogador.variacao_num > 0) {
				// valorizou
				pontuacao -= 20;

				if (jogador.preco_num > 0 && jogador.preco_num < 5) {
					pontuacao -= 0;
				} else if (jogador.preco_num >= 5 && jogador.preco_num < 10) {
					pontuacao -= 1;
				} else if (jogador.preco_num >= 10 && jogador.preco_num < 15) {
					pontuacao -= 2;
				} else if (jogador.preco_num >= 15 && jogador.preco_num < 20) {
					pontuacao -= 3;
				} else if (jogador.preco_num >= 20 && jogador.preco_num < 25) {
					pontuacao -= 4;
				} else if (jogador.preco_num >= 25) {
					pontuacao -= 5;
				}
			} else if (jogador.variacao_num < 0){
				// desvalorizou
				pontuacao += 20;

				if (jogador.preco_num > 0 && jogador.preco_num < 5) {
					pontuacao += 5;
				} else if (jogador.preco_num >= 5 && jogador.preco_num < 10) {
					pontuacao += 4;
				} else if (jogador.preco_num >= 10 && jogador.preco_num < 15) {
					pontuacao += 3;
				} else if (jogador.preco_num >= 15 && jogador.preco_num < 20) {
					pontuacao += 2;
				} else if (jogador.preco_num >= 20 && jogador.preco_num < 25) {
					pontuacao += 1;
				} else if (jogador.preco_num >= 25) {
					pontuacao += 0;
				}
			}

			if (jogador.media_num > 0) {
				pontuacao += 5;

				if (jogador.media_num < 3.5) {
					pontuacao += 4;
				} else if (jogador.media_num < 5) {
					pontuacao += 3;
				} else if (jogador.media_num < 9) {
					pontuacao += 2;
				} else if (jogador.media_num >= 9) {
					pontuacao += 1;
				}
			} else if (jogador.media_num < 0) {
				pontuacao -= 5;

				if (jogador.media_num > -3.5) {
					pontuacao -= 1;
				} else if (jogador.media_num > -5) {
					pontuacao -= 2;
				} else if (jogador.media_num > -9) {
					pontuacao -= 3;
				} else if (jogador.media_num <= -9) {
					pontuacao -= 4;
				}
			}

			if (jogador.clube_id === jogador.partida.clube_casa_id) {
				// mandante do jogo
				pontuacao += 5;
				jogador.partida.mandante = 1;

				var m = jogador.partida.clube_casa_posicao;
				var v = jogador.partida.clube_visitante_posicao;
				var coeficiente = v - m;

				// a frente do visitante na tabela
				if (v > m) {
					// ganha pontos
					pontuacao += ((coeficiente + 1) / 2) * (1 - ((m - 1) * 0.05));
				} else {
					// perde pntos
					pontuacao += ((coeficiente - 1) / 2) * (1 + ((m - 1) * 0.05));
				}
			} else {
				// visitante do jogo
				pontuacao -= 5;
				jogador.partida.mandante = 0;

				var m = jogador.partida.clube_casa_posicao;
				var v = jogador.partida.clube_visitante_posicao;
				var coeficiente = m - v;

				// atras do mandante na tabela
				if (v > m) {
					// perde pontos
					pontuacao += (((coeficiente - 1) / 2)) * (1 + ((v - 1) * 0.05));
				} else {
					// ganha pontos
					pontuacao += (((coeficiente + 1) / 2)) * (1 - ((v - 1) * 0.05));
				}
			}

			jogador.pontuacao = pontuacao;
			retorno.push(jogador);
		}
 
		retorno.sort(function(a,b){
			// maior pontuacao primeiro
			if (a.pontuacao > b.pontuacao) {
				return -1;
			}
			if (a.pontuacao < b.pontuacao) {
				return 1;
			}

			// em caso de empate, quem desvalorizou mais primeiro
			if (a.variacao_num < b.variacao_num) {
				return -1;
			}
			if (a.variacao_num > b.variacao_num) {
				return 1;
			}

			return 0;
		});

		if (limit === undefined) {
			return retorno;
		} else {
			return retorno.slice(0,limit);
		}
	}
}